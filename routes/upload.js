const express = require("express");
const fileUpload = require("express-fileupload");
const app = express();

const Usuario = require("../models/usuario");

//borrar archivo
const fs = require('fs')
const path = require('path')

//default options
app.use(fileUpload({ useTempFiles: true}));

app.put("/upload", function(req, res){
    if (!req.files) {
        return res.status(400).json({
            ok: false,
            err: {
                message: "No se ha seleccionado ningun archivo",
            },
        });
    }

    let archivo = req.files.archivo;

    let nombreArchivoCortado = archivo.name.split(".");
    let extension = nombreArchivoCortado[nombreArchivoCortado.length - 1];

    //extensiones permitidas
    let extensionesValidas = ["png","jpg","gif","jpeg"];

    if (extensionesValidas.indexOf(extension) < 0) {
        return res.status(400).json({
            ok:false,
            err: {
                message :
                "Las extensiones permitidas son " + extensionesValidas.join(", "),
                ext: extension
            }, 
        });
    }

    archivo.mv("uploads/filename.jpg", (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }
        res.json({
            ok: true,
            message: "Imagen subida correctamente", 
        });
    });
});

app.put("/upload/:tipo/:id", function(req, res) {
    let tipo = req.params.tipo;
    let id = req.params.id;

    if (!req.files) {
        return res.status(400).json({
            ok: false,
            err: {
                message: "No se ha seleccionado ningun archivo",
            },
        });
    }

    //validar tipo
    let tiposValidos = ["productos", "usuarios"];

    if(tiposValidos.indexOf(tipo)< 0) {
        return res.status(400).json({
            ok:false,
            err: {
                message :
                "Los tipos permitidos son " + tiposValidos.join(", "),
            }, 
        });
    }
    let archivo = req.files.archivo;

    let nombreArchivoCortado = archivo.name.split(".");
    let extension = nombreArchivoCortado[nombreArchivoCortado.length - 1];

    //extensiones permitidas
    let extensionesValidas = ["png","jpg","gif","jpeg"];

    if (extensionesValidas.indexOf(extension) < 0) {
        return res.status(400).json({
            ok:false,
            err: {
                message :
                "Las extensiones permitidas son " + extensionesValidas.join(", "),
                ext: extension
            }, 
        });
    }

    //Cambiar nombre al archivo
    let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extension}`;

    archivo.mv(`uploads/${tipo}/${nombreArchivo}`, (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }
        imagenUsuario(id, res, nombreArchivo);
    });
});

function imagenUsuario(id, res, nombreArchivo) {
    Usuario.findById(id, (err, usuarioDB) => {
        if(err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }

        if (!usuarioDB) {
            return res.status(400).json({
                ok:false,
                err: {
                    message :
                    "Usuario no existe",
                }, 
            });
        }

        let pathImagen = path.resolve(__dirname, `../uploads/usuarios/${usuarioDB.img}`)
        console.log(pathImagen)
        if(fs.existsSync(pathImagen)){
            fs.unlinkSync(pathImagen)
        }

        usuarioDB.img = nombreArchivo;

        usuarioDB.save((err, usuarioGuardado) => {
            res.json({
                ok: true,
                usuario: usuarioGuardado,
                img: nombreArchivo,
            });
        });
    });
}

module.exports = app;